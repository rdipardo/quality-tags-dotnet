﻿/**
 * CustomersControllerTest.cs
 * 
 * Unit tests
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.12: Created
 */

using QualityTagsAlertSystem.Controllers;
using QualityTagsAlertSystem.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QualityTagsAlertSystem.Tests
{
    [TestFixture]
    class CustomersControllerTest
    {
        private CustomersController _controllerUnderTest;
        private readonly int tagId_1 = 1001;
        private readonly int tagId_2 = 1002;

        /// <summary>
        /// Initializes affected models, controllers, services, etc.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var httpContext = Substitute.For<HttpContext>();
            var request = Substitute.For<HttpRequest>();
            var dbContext = Substitute.For<QualityTagsContext>();
            var tagsData = new MockDbSet<QualityIssues>();
            var problemSources = new MockDbSet<ProblemSources>();
            var partsData = new MockDbSet<Parts>();
            var customerData = new MockDbSet<Customers>();
            var contactsData = new MockDbSet<CustomerContacts>();
            var sortersData = new MockDbSet<CustomerSorters>();
            var salesRepsData = new MockDbSet<CustomerSalesReps>();
            var plantsData = new MockDbSet<PlantNames>();

            dbContext.QualityIssues.Returns(tagsData);
            dbContext.ProblemSources.Returns(problemSources);
            dbContext.Parts.Returns(partsData);
            dbContext.Customers.Returns(customerData);
            dbContext.CustomerContacts.Returns(contactsData);
            dbContext.CustomerSorters.Returns(sortersData);
            dbContext.CustomerSalesReps.Returns(salesRepsData);
            dbContext.PlantNames.Returns(plantsData);

            httpContext.Request.Returns(request);

            _controllerUnderTest = new CustomersController(dbContext)
            {
                TempData = new MockTempDataDictionary(),
                ControllerContext = new ControllerContext()
                {
                    HttpContext = httpContext,

                }
            };

            problemSources.AddRange(new[]
            {
                new ProblemSources()
                {
                    ProblemSourceCode = "IN",
                    ProblemSource = "Internal"
                },
                new ProblemSources()
                {
                    ProblemSourceCode = "EX",
                    ProblemSource = "External"
                },
            });

            tagsData.AddRange(new[]
            {
                new QualityIssues()
                {
                    Id = tagId_1,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    IssuedBy = "moi-m\u00CAme",
                    PartId = "11-456",
                    Reason = "none",
                    Operation = "Scrap",
                    CustomerId = 100,
                    QualityAtag = true
                },
                new QualityIssues()
                {
                    Id = tagId_2,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    PartId = "50-0455-FM",
                    Reason = "Missed Operation",
                    Operation = "GP12",
                    IssuedBy = "vous-m\u00CAme",
                    TpcTag = true
                }
            });

            customerData.Add(new Customers()
            {
                CustomerId = 100,
                CustName = "Goeman's",
                CustLocation = "Windsor",
                Current = true
            });

            contactsData.Add(new CustomerContacts()
            {
                CustContId = 1,
                CustomerId = 100,
                ContactFirstName = "John",
                ContactLastName = "Lennon"
            });

            sortersData.Add(new CustomerSorters()
            {
                CustSortId = 2,
                CustomerId = 100,
                SortContactFirstName = "George",
                SortContactLastName = "Harrsion"
            });

            salesRepsData.Add(new CustomerSalesReps()
            {
                CustSalesId = 3,
                CustomerId = 100,
                SalesRepFirstName = "Ringo",
                SalesRepLastName = "Star"
            });

            partsData.Add(new Parts()
            {
                PartId = "11-456",
                PlantNumber = 11,
                Description = "Part 1"
            });


            plantsData.Add(new PlantNames()
            {
                PlantId = 11,
                PlantDescrpition = "Big plant"
            });
        }

        /// <summary>
        /// This is a test method that checks if the customer issues page returns the view for customer issues.
        /// </summary>
        [Test]
        public void CustomerIssues_Action_Returns_Correct_View_Model()
        {
            var result = _controllerUnderTest.CustomerIssues().Result as ViewResult;
            var viewModel = result.ViewData.Model as IOrderedEnumerable<CustomerIssue>;

            Assert.IsTrue(viewModel.OfType<CustomerIssue>().FirstOrDefault() is CustomerIssue);
        }

        /// <summary>
        /// Test if the expected View Model is sent to Customer Contacts View.
        /// </summary>
        [Test]
        public void Contacts_Action_Returns_Correct_View_Model()
        {
            var result = _controllerUnderTest.Contacts().Result as ViewResult;
            var viewModel = result.ViewData.Model as List<IGrouping<string, CustomerContact>>;

            Assert.IsTrue(viewModel.FirstOrDefault() is IEnumerable<CustomerContact>);
        }

        /// <summary>
        /// Test if the expected View Model is sent to Customer Sorters View.
        /// </summary>
        [Test]
        public void Sorters_Action_Returns_Correct_View_Model()
        {
            var result = _controllerUnderTest.Sorters().Result as ViewResult;
            var viewModel = result.ViewData.Model as List<IGrouping<string, CustomerSorter>>;

            Assert.IsTrue(viewModel.FirstOrDefault() is IEnumerable<CustomerSorter>);
        }
    }
}
