/**
 * HomeControllerTest.cs
 * 
 * Unit tests
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.20: Created
 */

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using QualityTagsAlertSystem.Controllers;

namespace QualityTagsAlertSystem.Tests
{
    [TestFixture]
    class HomeControllerTest
    {
        private HomeController testController;

        [SetUp]
        public void Setup()
        {
            testController = new HomeController()
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext()
                }
            };
        }

        [Test]
        public void Index_Action_Serves_Home_Page()
        {
            var result = testController.Index() as ViewResult;

            Assert.IsNotNull(result);
            StringAssert.AreEqualIgnoringCase("Welcome", result.ViewData["PageName"].ToString());
        }

        [Test]
        public void About_Action_Serves_About_Page()
        {
            var result = testController.About() as ViewResult;

            Assert.IsNotNull(result);
        }
    }
}