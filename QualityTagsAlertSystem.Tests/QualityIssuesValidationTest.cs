﻿/**
 * QualityIssuesValidationTest.cs
 * 
 * Unit tests
 * 
 * Revision History
 *     Robert Di Pardo, 2020.03.21: Created
 *     "Bao Khanh Nguyen", 2020.04.10: Expanded test suite
 *		 
 */

using QualityTagsAlertSystem.Models;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Tests
{
    [TestFixture]
    class QualityIssuesValidationTest
    {
        [Test]
        public void QualityIssues_Requires_IssuedBy()
        {
            QualityIssues invalidModel = new QualityIssues()
            {
                ProblemType = "EX",
                PartId = "1102-8537",
                Reason = "Cracked",
                IssuedBy = string.Empty,
                QualityAtag = true,
                DispositionCustomer = "\r\n"
            };

            List<ValidationResult> results = ValidateModel(invalidModel);
            Assert.AreEqual(2, results.Count);
            StringAssert.AreEqualIgnoringCase("Please fill in 'Issued By'!", results[0].ErrorMessage);
            StringAssert.AreEqualIgnoringCase("Please select a disposition at customer!", results[1].ErrorMessage);
        }

        [Test]
        public void QualityIssues_Requires_Reason()
        {
            QualityIssues invalidModel = new QualityIssues()
            {
                ProblemType = "EX",
                PartId = "1102-8537",
                Reason = string.Empty,
                IssuedBy = "vous-m\u00CAme",
                QualityAtag = true,
                DispositionCustomer = "\r\n"
            };

            List<ValidationResult> results = ValidateModel(invalidModel);
            Assert.AreEqual(2, results.Count);
            StringAssert.AreEqualIgnoringCase("Please select a reason!", results[0].ErrorMessage);
            StringAssert.AreEqualIgnoringCase("Please select a disposition at customer!", results[1].ErrorMessage);
        }

        [Test]
        public void QualityIssues_Given_Valid_External_Hold_Tag_Validates()
        {
            QualityIssues validModel = new QualityIssues()
            {
                ProblemType = "EX",
                PartId = "1102-8537",
                Reason = "Chips/Flakes",
                IssuedBy = "yours truly",
                HoldTag = true,
                Operation = "Sinter"
            };

            List<ValidationResult> results = ValidateModel(validModel);
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        public void QualityIssues_Given_Invalid_External_Quality_Alert_Fails_To_Validate()
        {
            QualityIssues invalidModel = new QualityIssues()
            {
                ProblemType = "EX",
                PartId = string.Empty,
                Reason = "Cracked",
                IssuedBy = "vous-m\u00CAme",
                QualityAtag = true,
                DispositionCustomer = "\r\n"
            };

            List<ValidationResult> results = ValidateModel(invalidModel);
            Assert.AreEqual(2, results.Count);
            StringAssert.AreEqualIgnoringCase("Please select a part number!", results[0].ErrorMessage);
            StringAssert.AreEqualIgnoringCase("Please select a disposition at customer!", results[1].ErrorMessage);
        }

        [Test]
        public void QualityIssues_Given_Valid_TPC_Tag_Validates()
        {
            QualityIssues invalidModel = new QualityIssues()
            {
                ProblemType = "EX",
                PartId = "1102-8537",
                Reason = "Cracked",
                IssuedBy = "vous-m\u00CAme",
                TpcTag = false,
                DispositionCustomer = "\r\n"
            };

            List<ValidationResult> results = ValidateModel(invalidModel);
            Assert.AreEqual(0, results.Count);
        }

        /// <summary>
        /// Adapted from <see href="https://visualstudiomagazine.com/articles/2015/06/19/tdd-asp-net-mvc-part-4-unit-testing.aspx"/>.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<ValidationResult> ValidateModel(QualityIssues model)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext validationContext = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, validationContext, results, true);
            (model as IValidatableObject).Validate(validationContext);

            return results;
        }
    }
}
