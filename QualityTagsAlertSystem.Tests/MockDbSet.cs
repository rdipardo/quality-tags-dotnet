﻿/**
 * DummyDbSet.cs
 * 
 * Mock-up adapted from Miguel Fernández Corral's answer to https://social.msdn.microsoft.com/Forums/en-US/52a7c9f9-ec59-4e36-8728-bb84baf1c5e1/how-to-create-a-custom-idbsetltgt-that-always-filters-on-a-value-given-in-the-constructor, 
 * with a saving hack suggested by malik's answer to https://stackoverflow.com/questions/41091155/entity-framework-7-create-in-memory-implementation-of-dbset.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.20: Created
 *     "    ",          2022.11.28: Provided a stub EntityType accessor
 */

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QualityTagsAlertSystem.Tests
{
    /// <summary>
    /// Mini implementation of the abstract <see cref="DbSet{TEntity}"/> class.
    /// </summary>
    internal sealed class MockDbSet<TEntity> : DbSet<TEntity>, IOrderedQueryable<TEntity>, IOrderedQueryable, IQueryable<TEntity>, IQueryable, IEnumerable<TEntity>, IEnumerable, IListSource where TEntity : class
    {
        private readonly ObservableCollection<TEntity> _entities;

        public MockDbSet()
        {
            _entities = new ObservableCollection<TEntity>();
        }

        public override EntityEntry<TEntity> Add(TEntity entity)
        {
            _entities.Add(entity);
            return entity as EntityEntry<TEntity>;
        }

        public override void AddRange(params TEntity[] entities)
        {
            foreach (var item in entities)
            {
                _entities.Add(item);
            }
        }

        public override EntityEntry<TEntity> Remove(TEntity entity)
        {
            _entities.Remove(entity);
            return entity as EntityEntry<TEntity>;
        }

        public override EntityEntry<TEntity> Attach(TEntity entity) => entity as EntityEntry<TEntity>;

        public override TEntity Find(params object[] keyValues) => _entities.FirstOrDefault();

        public override async ValueTask<TEntity> FindAsync(params object[] keyValues) => await Task.Run(() => _entities.FirstOrDefault());

        public override LocalView<TEntity> Local => _entities as LocalView<TEntity>;

        IEnumerator<TEntity> IEnumerable<TEntity>.GetEnumerator() => _entities.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => _entities.GetEnumerator();

        Type IQueryable.ElementType => _entities.AsQueryable().ElementType;

        Expression IQueryable.Expression => _entities.AsQueryable().Expression;

        IQueryProvider IQueryable.Provider => _entities.AsQueryable().Provider;

        bool IListSource.ContainsListCollection => true;

        public override IEntityType EntityType => throw new NotImplementedException();

        IList IListSource.GetList() => _entities.ToList();
    }
}
