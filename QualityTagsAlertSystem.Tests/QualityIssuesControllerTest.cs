﻿/**
 * QualityIssuesControllerTest.cs
 * 
 * Unit tests
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.20: Created
 *     Chackaphope Ying Yong, 2020.04.10: Added documentation 
 */

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using NSubstitute;
using NUnit.Framework;
using QualityTagsAlertSystem.Controllers;
using QualityTagsAlertSystem.Models;
using QualityTagsAlertSystem.Services;
using System;
using System.Collections.Generic;
using System.IO;

namespace QualityTagsAlertSystem.Tests
{
    [TestFixture]
    class QualityIssuesControllerTest
    {
        private QualityIssuesController _controllerUnderTest;
        private readonly int tagId_1 = 1001;
        private readonly int tagId_2 = 1002;

        /// <summary>
        /// Initializes affected models, controllers, services, etc.
        /// </summary>
        /// <remarks>
        /// Mock <see cref="IUrlHelper"/> adapted from <see href="https://stackoverflow.com/questions/42212066/net-core-url-action-mock-how-to"/>.
        /// </remarks>
        [SetUp]
        public void Setup()
        {
            var emailService = new EmailService(
                            Substitute.For<SmtpConfiguration>(),
                            Substitute.For<ILogger<EmailService>>());
            var httpContext = Substitute.For<HttpContext>();
            var request = Substitute.For<HttpRequest>();
            var dbContext = Substitute.For<QualityTagsContext>();
            var tagsData = new MockDbSet<QualityIssues>();
            var problemSources = new MockDbSet<ProblemSources>();
            var partsData = new MockDbSet<Parts>();
            var reasonsData = new MockDbSet<HtpcReasons>();
            var processData = new MockDbSet<Depts>();
            var machinesData = new MockDbSet<Machines>();
            var customerData = new MockDbSet<Customers>();
            var plantsData = new MockDbSet<PlantNames>();
            var dispositionsData = new MockDbSet<CustomerDispositions>();
            var pictureData = new MockDbSet<Blobs>();
            var attachmentData = new MockDbSet<Attachments>();
            var storageLocationData = new MockDbSet<StorageLocations>();

            dbContext.QualityIssues.Returns(tagsData);
            dbContext.ProblemSources.Returns(problemSources);
            dbContext.Parts.Returns(partsData);
            dbContext.HtpcReasons.Returns(reasonsData);
            dbContext.Depts.Returns(processData);
            dbContext.Machines.Returns(machinesData);
            dbContext.Customers.Returns(customerData);
            dbContext.PlantNames.Returns(plantsData);
            dbContext.CustomerDispositions.Returns(dispositionsData);
            dbContext.Blobs.Returns(pictureData);
            dbContext.Attachments.Returns(attachmentData);
            dbContext.StorageLocations.Returns(storageLocationData);

            request.Method.Returns("POST");
            request.Headers.Returns(
                new HeaderDictionary { { "X-Requested-With", "XMLHttpRequest" } });
            request.ContentType.Returns("multipart/form-data");

            var form = new Dictionary<string, StringValues>()
            {
                {"tagCreator", new StringValues(new Guid().ToString()) },
                {"photo01", new StringValues(Path.Combine(Path.GetTempPath(), "dummy.png") )},
                {"photo02", new StringValues(Path.Combine(Path.GetTempPath(), "dummy.jpeg")) }
            };

            request.Form.Returns(new FormCollection(form));
            httpContext.Request.Returns(request);

            var urlHelper = Substitute.For<IUrlHelper>();
            urlHelper
                .Action(Arg.Any<UrlActionContext>())
                .Returns("/QualityTagsAlertSystem");

            _controllerUnderTest = new QualityIssuesController(dbContext, emailService)
            {
                TempData = new MockTempDataDictionary(),
                Url = urlHelper,
                ControllerContext = new ControllerContext()
                {
                    HttpContext = httpContext,
                }
            };

            problemSources.AddRange(new[]
            {
                new ProblemSources()
                {
                    ProblemSourceCode = "IN",
                    ProblemSource = "Internal"
                },
                new ProblemSources()
                {
                    ProblemSourceCode = "EX",
                    ProblemSource = "External"
                },
            });

            tagsData.AddRange(new[]
            {
                new QualityIssues()
                {
                    Id = tagId_1,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    IssuedBy = "moi-m\u00CAme",
                    PartId = "11-456",
                    Reason = "none",
                    Operation = "Scrap",
                    CustomerId = 100,
                    QualityAtag = true
                },
                new QualityIssues()
                {
                    Id = tagId_2,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    PartId = "50-0455-FM",
                    Reason = "Missed Operation",
                    Operation = "GP12",
                    IssuedBy = "vous-m\u00CAme",
                    TpcTag = true
                }
            });

            customerData.Add(new Customers()
            {
                CustomerId = 100,
                CustName = "Goeman's",
                CustLocation = "Windsor"
            });

            partsData.Add(new Parts()
            {
                PartId = "11-456",
                PlantNumber = 11,
                Description = "Part 1"
            });


            plantsData.Add(new PlantNames()
            {
                PlantId = 11,
                PlantDescrpition = "Big plant"
            });
        }

        /// <summary>
        /// This is the test that is performed to show that the index returns the tag list
        /// </summary>
        [Test]
        public void Index_Action_Returns_Tags_List()
        {
            var result = _controllerUnderTest.Index() as ViewResult;

            Assert.IsTrue(result.ViewData["Tags"] is SelectList);
        }

        /// <summary>
        /// This test id the print button will return the correct tag view
        /// </summary>
        [Test]
        public void Print_Action_Serves_Correct_Tag_View()
        {
            var result = _controllerUnderTest.Print(tagId_1, null).Result as ViewResult;
            var viewModel = (TagReport)result.ViewData.Model;

            Assert.IsTrue(viewModel.Id == tagId_1);
            Assert.IsTrue(viewModel.Template == TagTemplate.QualityAlertExternal);
        }

        /// <summary>
        /// This will test if the create method pulls the right attribute from the controller's get/set attribute
        /// </summary>
        [Test]
        public void Create_Action_Responds_To_Get_Method_With_Correct_Model_Type()
        {
            var result = _controllerUnderTest.Create("IN").Result as ViewResult;
            var viewModel = (QualityIssues)result.ViewData.Model;

            StringAssert.AreEqualIgnoringCase("IN", viewModel.ProblemType);
        }

        /// <summary>
        /// this will test if the program runs correctly into the catch method if the model state is invalid
        /// </summary>
        [Test]
        public void Create_When_Model_Is_Invalid_Returns_ModelState()
        {
            var tag = new QualityIssues() { Id = 1, ProblemType = "IN" };
            _controllerUnderTest.ModelState.AddModelError("PartId", "No part selected!");

            var result = _controllerUnderTest.Create(tag).Result as ViewResult;

            Assert.AreEqual(1, result.ViewData.ModelState.ErrorCount);
        }

        /// <summary>
        /// Test if the create method will return to the index view when the issue is successfully created
        /// </summary>
        [Test]
        public void Create_When_Model_Is_Valid_Redirects_To_Index_View()
        {
            var result = _controllerUnderTest.Create(new QualityIssues()).Result as RedirectToActionResult;

            Assert.IsTrue(_controllerUnderTest.ModelState.IsValid);
            StringAssert.AreEqualIgnoringCase("Index", result.ActionName);
        }

        /// <summary>
        /// Test if the edit will get the id from the controller, then redirect the user to print screen
        /// </summary>
        [Test]
        public void Edit_Responds_To_Get_Method_With_Redirect_To_Print()
        {
            var result = _controllerUnderTest.Edit(tagId_2).Result as RedirectToActionResult;

            StringAssert.AreEqualIgnoringCase("Print", result.ActionName);
            Assert.AreEqual(tagId_2, result.RouteValues["id"]);
        }

        /// <summary>
        /// Test if the edit method will not continue to work if the model is invalid
        /// </summary>
        [Test]
        public void Edit_When_Model_Is_Invalid_Returns_ModelState()
        {
            string returnUrl = "/QualityIssues";
            var tagDb = new MockDbSet<QualityIssues>();
            tagDb.Add(new QualityIssues() { Id = 999, ProblemType = "EX" });

            _controllerUnderTest.ModelState.AddModelError("IssuedBy", "'Issued by is empty!'");
            _controllerUnderTest.ModelState.AddModelError("Operation", "No operation selected!");

            var result = _controllerUnderTest.Edit(999, returnUrl, tagDb.Find(999)).Result as ViewResult;
            var errors = result.ViewData.ModelState.ErrorCount;

            Assert.AreEqual(2, errors);
        }

        /// <summary>
        /// Test if a successful edit model state is valid and will continue to run through the code properly
        /// </summary>
        [Test]
        public void Edit_When_Model_Is_Valid_Redirects_To_Index_Action()
        {
            string returnUrl = "/QualityIssues";

            var result = _controllerUnderTest.Edit(tagId_1, returnUrl, new QualityIssues()).Result as RedirectToActionResult;

            Assert.IsTrue(_controllerUnderTest.ModelState.IsValid);
            StringAssert.AreEqualIgnoringCase("Index", result.ActionName);
        }

        /// <summary>
        /// Test if the expected View Model is sent to Hold Tag Sign Off form.
        /// </summary>
        [Test]
        public void SignOff_Action_Returns_Correct_View_Model()
        {
            var result = _controllerUnderTest.SignOffHoldTag().Result as ViewResult;
            StringAssert.AreEqualIgnoringCase("OpenHoldTag", result.ViewData.Model.GetType().Name);
        }
    }
}
