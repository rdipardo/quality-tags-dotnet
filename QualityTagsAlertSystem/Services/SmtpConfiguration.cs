﻿/**
 * SmtpConfiguration.cs
 * 
 * Encapsulates configuration settings of the SMTP client.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 *     "    "         , 2020.04.07: Added new properties
 */

namespace QualityTagsAlertSystem.Services
{
    /// <summary>
    /// Provides encapsulation for the SMTP client's configuration settings.
    /// </summary>
    public interface ISmtpConfiguration
    {
        /// <summary>
        /// Gets or sets the host name or IP of the SMTP client.
        /// </summary>
        string SmtpServer { get; }

        /// <summary>
        /// Gets or sets the port number the SMTP client listens on.
        /// </summary>
        int SmtpPort { get; }

        /// <summary>
        /// Gets or sets the name attached to e-mails sent from the SMTP client.
        /// </summary>
        string MailerName { get; }

        /// <summary>
        /// Get's or sets the "From" address attached to e-mails sent from the SMTP client.
        /// </summary>
        string MailerAddress { get; }

        /// <summary>
        /// Only used by remote relays that support authentication.
        /// </summary>
        string SmtpUsername { get; }

        /// <summary>
        /// See <see cref="SmtpUsername"/>.
        /// </summary>
        string SmtpPassword { get; }
    }

    /// <summary>
    /// Implements the <see cref="ISmtpConfiguration"/> interface.
    /// </summary>
    public class SmtpConfiguration : ISmtpConfiguration
    {
        /// <inheritdoc/>
        public string SmtpServer { get; set; }

        /// <inheritdoc/>
        public int SmtpPort { get; set; }

        /// <inheritdoc/>
        public string MailerName { get; }

        /// <inheritdoc/>
        public string MailerAddress { get; set; }

        /// <inheritdoc/>
        public string SmtpUsername { get; set; }

        /// <inheritdoc/>
        public string SmtpPassword { get; set; }
    }
}
