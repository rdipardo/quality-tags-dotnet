﻿/**
 * ViewRenderService.cs
 * 
 * Adapted from https://github.com/scottsauber/RazorHtmlEmails
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 */

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QualityTagsAlertSystem.Services
{
    /// <summary>
    /// Provides the capability to programmatically render a View's HTML content without serving it to the browser.
    /// </summary>
    public interface IViewRenderService
    {
        /// <summary>
        /// Renders a View and returns its HTML content as a string.
        /// </summary>
        /// <typeparam name="TModel"> The type of the given <paramref name="model"/>.</typeparam>
        /// <param name="viewName"> The name of the View to render.</param>
        /// <param name="model"> The model passed to the View identified by <paramref name="viewName"/>.</param>
        /// <returns> The rendered content of the View identified by <paramref name="viewName"/>.</returns>
        Task<string> RenderToStringAsync<TModel>(string viewName, TModel model);
    }

    /// <summary>
    /// An implementation of the <see cref="IViewRenderService"/> interface.
    /// </summary>
    /// <remarks>
    /// Implementation adapted from <see href="https://github.com/scottsauber/RazorHtmlEmails"/>.
    /// </remarks>
    public class ViewRenderService : IViewRenderService
    {
        private readonly IRazorViewEngine _viewEngine;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IServiceProvider _serviceProvider;
        private readonly IHttpContextAccessor _httpContext;

        /// <summary>
        /// Constructs a new <see cref="ViewRenderService"/> from the given parameters.
        /// </summary>
        /// <param name="viewEngine"> An initialized implementation of the <see cref="IRazorViewEngine"/> interface.</param>
        /// <param name="tempDataProvider"> An initialized implementation of the <see cref="ITempDataProvider"/> interface.</param>
        /// <param name="serviceProvider"> An initialized implementation of the <see cref="IServiceProvider"/> interface.</param>
        /// <param name="httpContext"> An initialized implementation of the <see cref="IHttpContextAccessor"/> interface.</param>
        public ViewRenderService(
            IRazorViewEngine viewEngine,
            ITempDataProvider tempDataProvider,
            IServiceProvider serviceProvider,
            IHttpContextAccessor httpContext)
        {
            _viewEngine = viewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
            _httpContext = httpContext;
        }

        /// <inheritdoc/>
        public async Task<string> RenderToStringAsync<TModel>(string viewName, TModel model)
        {
            var actionContext = new ActionContext(_httpContext.HttpContext, new RouteData(), new ActionDescriptor());
            var urlHelper = new UrlHelper(actionContext);
            actionContext = urlHelper.ActionContext;

            var view = FindView(actionContext, viewName);

            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext(actionContext, view,
                    new ViewDataDictionary<TModel>(
                        metadataProvider: new EmptyModelMetadataProvider(),
                        modelState: new ModelStateDictionary())
                    {
                        Model = model
                    },
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    output,
                    new HtmlHelperOptions());

                await view.RenderAsync(viewContext);

                return output.ToString();
            }
        }

        /// <summary>
        /// Queries the application's View directory for the View identified by <paramref name="viewName"/>.
        /// </summary>
        /// <param name="actionContext"> An initialized instance of <see cref="ActionContext"/>.</param>
        /// <param name="viewName"> The name of a View page.</param>
        /// <returns> The matching View, if any.</returns>
        /// <exception cref="InvalidOperationException"> Thrown if no View with the given <paramref name="viewName"/> can be found.</exception>
        private IView FindView(ActionContext actionContext, string viewName)
        {
            var getViewResult = _viewEngine.GetView(executingFilePath: null, viewPath: viewName, isMainPage: false);
            if (getViewResult.Success)
            {
                return getViewResult.View;
            }

            var findViewResult = _viewEngine.FindView(actionContext, viewName, isMainPage: false);
            if (findViewResult.Success)
            {
                return findViewResult.View;
            }

            var searchedLocations = getViewResult.SearchedLocations.Concat(findViewResult.SearchedLocations);
            var errorMessage = string.Join(
                Environment.NewLine,
                new[] { $"Unable to find view '{viewName}'. The following locations were searched:" }.Concat(searchedLocations)); ;

            throw new InvalidOperationException(errorMessage);
        }
    }
}

