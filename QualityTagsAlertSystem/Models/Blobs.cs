﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class Blobs
    {
        public int Id { get; set; }
        public int Path { get; set; }
        public string Key { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public long? Bytes { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual StorageLocations PathNavigation { get; set; }
    }
}
