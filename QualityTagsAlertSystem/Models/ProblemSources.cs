﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class ProblemSources
    {
        public string ProblemSourceCode { get; set; }
        public string ProblemSource { get; set; }
    }
}
