﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class StorageLocations
    {
        public StorageLocations()
        {
            Attachments = new HashSet<Attachments>();
            Blobs = new HashSet<Blobs>();
        }

        public int Id { get; set; }
        public string Path { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual ICollection<Attachments> Attachments { get; set; }
        public virtual ICollection<Blobs> Blobs { get; set; }
    }
}
