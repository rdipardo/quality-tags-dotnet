﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class PlantNames
    {
        public int PlantId { get; set; }
        public string PlantName { get; set; }
        public string PlantDescrpition { get; set; }
        public int? Area { get; set; }
    }
}
