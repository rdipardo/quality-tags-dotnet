﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class PartsCustomer
    {
        public int PartCustId { get; set; }
        public string PartId { get; set; }
        public int? CustId { get; set; }
    }
}
