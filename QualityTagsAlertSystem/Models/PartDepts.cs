﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class PartDepts
    {
        public int PartDeptId { get; set; }
        public string PartId { get; set; }
        public int? DeptId { get; set; }
        public int? Sequence { get; set; }
        public decimal Cost { get; set; }
        public double WeightLbs { get; set; }
        public int? Area { get; set; }
        public DateTime? DateCost { get; set; }
    }
}
