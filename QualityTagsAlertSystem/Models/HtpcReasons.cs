﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class HtpcReasons
    {
        public int Id { get; set; }
        public string Reason { get; set; }
    }
}
