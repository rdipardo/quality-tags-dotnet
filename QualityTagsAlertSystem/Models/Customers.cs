﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class Customers
    {
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
    }
}
