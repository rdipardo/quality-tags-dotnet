﻿/**
 * PasswordUpdate.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.10: Created
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of a password change and defines criteria for acceptable passwords and emails.
    /// </summary>
    public class PasswordUpdate : IValidatableObject
    {
        /// <summary>
        /// Gets or sets the email of the user who is changing passwords.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [RegularExpression(@"^.+\@\w+(\.\w{2,})+$", ErrorMessage = "Invalid e-mail address.")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the old password of the user who is changing passwords.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(30)]
        public string OldPassword { get; set; }

        /// <summary>
        /// Gets or sets the new password of the user who is changing passwords.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [StringLength(30)]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the repeated password entry of the user who is changing passwords.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [StringLength(30)]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmNewPassword { get; set; }

        /// <inheritdoc/>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            string password = OldPassword?.Trim();
            string newPassword = NewPassword?.Trim();
            string confirmNewPassword = ConfirmNewPassword?.Trim();


            if (string.IsNullOrEmpty(Email?.Trim()))
            {
                yield return new ValidationResult("Please enter an e-mail address.",
                    new[] { nameof(Email) });
            }

            if (string.IsNullOrEmpty(newPassword) || newPassword.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(NewPassword) });
            }

            if (string.IsNullOrEmpty(confirmNewPassword) || confirmNewPassword.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(ConfirmNewPassword) });
            }

            yield return ValidationResult.Success;
        }
    }
}
