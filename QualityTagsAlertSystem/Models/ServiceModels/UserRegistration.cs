﻿/**
 * UserRegistration.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 *     "    ",          2020.04.08: Added validation logic
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates a user's registration details and defines criteria for acceptable user names, passwords, and emails.
    /// </summary>
    public class UserRegistration : IValidatableObject
    {
        /// <summary>
        /// Gets or sets the username of a new or returning application user.
        /// </summary>
        [Required]
        [Display(Name = "User Name")]
        [StringLength(30)]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the email of a new or returning application user.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [RegularExpression(@"^.+\@\w+(\.\w{2,})+$", ErrorMessage = "Invalid e-mail address.")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password of a new or returning application user.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(30)]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the repeated password entry of a new or returning application user.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [StringLength(30)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <inheritdoc/>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            string userName = UserName?.Trim();
            string email = Email?.Trim();
            string password = Password?.Trim();
            string confirmPassword = ConfirmPassword?.Trim();

            if (string.IsNullOrEmpty(userName) || userName.Length > 30)
            {
                yield return new ValidationResult("User name must be 1-30 characters.",
                    new[] { nameof(UserName) });
            }

            if (string.IsNullOrEmpty(email))
            {
                yield return new ValidationResult("Please enter an e-mail address.",
                    new[] { nameof(Email) });
            }

            if (string.IsNullOrEmpty(password) || password.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(Password) });
            }

            if (string.IsNullOrEmpty(confirmPassword) || confirmPassword.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(ConfirmPassword) });
            }

            yield return ValidationResult.Success;
        }
    }
}