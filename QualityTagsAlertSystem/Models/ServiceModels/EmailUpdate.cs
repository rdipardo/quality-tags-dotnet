﻿/**
 * EmailUpdate.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.10: Created
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of an email change and defines criteria for acceptable emails.
    /// </summary>
    public class EmailUpdate : IValidatableObject
    {
        /// <summary>
        /// Gets or sets the new email of the user who is changing email.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "New Email")]
        [RegularExpression(@"^.+\@\w+(\.\w{2,})+$", ErrorMessage = "Invalid e-mail address.")]
        public string NewEmail { get; set; }

        /// <inheritdoc/>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if (string.IsNullOrEmpty(NewEmail?.Trim()))
            {
                yield return new ValidationResult("Please enter an e-mail address.",
                    new[] { nameof(NewEmail) });
            }

            yield return ValidationResult.Success;
        }
    }
}