﻿/**
 * Email.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 */

using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates the details of an email message.
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Constructs a new <see cref="Email"/> with a new <see cref="SenderAddressList"/> and <see cref="RecipientAddressList"/>.
        /// </summary>
        public Email()
        {
            SenderAddressList = new List<string>();
            RecipientAddressList = new List<string>();
        }

        /// <summary>
        /// Gets or sets the list of sender email addresses.
        /// </summary>
        public List<string> SenderAddressList { get; set; }

        /// <summary>
        /// Gets or sets the list of recipient email addresses.
        /// </summary>
        public List<string> RecipientAddressList { get; set; }

        /// <summary>
        /// Gets or sets the subject of the email message.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body of the email message.
        /// </summary>
        public string Content { get; set; }
    }
}
