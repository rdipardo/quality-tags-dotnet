﻿/**
 * EmailAddress.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 */

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of an email sender or recipient.
    /// </summary>
    public class EmailAddress
    {
        /// <summary>
        /// Gets or sets the name of the sender or recipient.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email address of the sender or recipient.
        /// </summary>
        public string Address { get; set; }
    }
}
