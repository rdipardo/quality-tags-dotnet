﻿/**
 * UserLogin.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 */

using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of a login attempt.
    /// </summary>
    public class UserLogin
    {
        /// <summary>
        /// Gets or sets the username used to log in.
        /// </summary>
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password used to log in.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the email used to log in. 
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the user's choice to persist their login session.
        /// </summary>
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
