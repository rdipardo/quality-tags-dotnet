﻿/**
 * CustomerInfo.cs
 *
 * Revision History
 *     Xinghua Li, 2020.04.09: Created
 */

using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates the details of a Customer's associated contacts and sale reps.
    /// </summary>
    public class CustomerContact
    {
        [Display(Name = "Contact Name")]
        public string ContactName => $"{ContactFirstName} {ContactLastName}";
        [Display(Name = "Phone")]
        public string ContactPhoneNumber { get; set; }
        [Display(Name = "Cell")]
        public string ContactCellNumber { get; set; }
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string SalesRepFirstName { get; set; }
        public string SalesRepLastName { get; set; }
        public string SalesRepPhoneNumber { get; set; }
    }
}
