﻿/**
 * PartValueCalculation.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.29: Created
 */

using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    ///  Encapsulates details of a part value calculation.
    /// </summary>
    public class PartValueCalculation
    {
        [Display(Name = "Part Number")]
        public string PartDeptId { get; set; }

        public string Department { get; set; }

        [DataType(DataType.Currency)]
        public decimal Cost { get; set; }

        [Display(Name = "Weight (lbs.)")]
        public double WeightLbs { get; set; }

        [Display(Name = "Cal. Cost")]
        [DataType(DataType.Currency)]
        public decimal TotalValue { get; set; }

        [Display(Name = "Cal. Weight")]
        public double TotalWeight { get; set; }

        public bool? RptScrap { get; set; }

        public string DepartmentState { get; set; }
    }
}
