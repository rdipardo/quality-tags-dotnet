﻿/**
 * OpenHoldTag.cs 
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.12: Created
 */

using System;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of an open Hold Tag.
    /// </summary>
    public partial class OpenHoldTag : SignedOffHoldTags
    {
        public new int? TagNumber { get => base.TagNumber; set => base.TagNumber = value; }

        public new DateTime? DateClosed { get => base.DateClosed; set => base.DateClosed = value; }

        public bool? Closed { get; set; }

        public int? Qty { get; set; }

        public string Comment { get; set; }
    }
}