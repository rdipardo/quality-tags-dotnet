﻿/**
 * CustomerIssue.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.16: Created
 */

using System;
using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates the details of a customer tag with its associated Customer and Plant.
    /// </summary>
    public class CustomerIssue : QualityIssues
    {
        /// <summary>
        /// Constructs a new <see cref="CustomerIssue"/> with details from associated records queried from the Tags, Customers and Plant tables.
        /// </summary>
        /// <param name="tag"> The <see cref="QualityIssues"/> record associated with this <see cref="CustomerIssue"/>.</param>
        /// <param name="customer"> The <see cref="Customers"/> record associated with this <see cref="CustomerIssue"/>.</param>
        /// <param name="plant"> The <see cref="PlantNames"/> record associated with this <see cref="CustomerIssue"/>.</param>
        public CustomerIssue(QualityIssues tag, Customers customer, PlantNames plant)
        {
            Id = tag.Id;
            ProblemType = tag.ProblemType;
            Date = tag.Date;
            DateIssued = tag.DateIssued;
            PartId = tag.PartId;
            CustomerRefNum = tag.CustomerRefNum;
            QualityAtag = tag.QualityAtag;
            HoldTag = tag.HoldTag;
            TpcTag = tag.TpcTag;
            SpecialInstWritten = tag.SpecialInstWritten;
            QualityMemo = tag.QualityMemo;
            Closed = tag.Closed;
            CustName = customer.CustName;
            CustLocation = customer.CustName;
            PlantName = plant.PlantName;
        }

        // override the caption, not the behaviour
        [Display(Name = "Date")]
        public new DateTime Date { get => base.Date; set => base.Date = value; }

        [Display(Name = "SI")]
        public new bool? SpecialInstWritten { get; set; }

        [Display(Name = "QA")]
        public new bool? QualityAtag { get; set; }
        [Display(Name = "HT")]
        public new bool? HoldTag { get; set; }

        [Display(Name = "TT")]
        public new bool? TpcTag { get; set; }

        [Display(Name = "QAI")]
        public new bool? QualityMemo { get; set; }

        [Display(Name = "Customer Name")]
        public string CustName { get; set; }

        [Display(Name = "Customer Location")]
        public string CustLocation { get; set; }

        [Display(Name = "Plant")]
        public string PlantName { get; set; }
    }
}
