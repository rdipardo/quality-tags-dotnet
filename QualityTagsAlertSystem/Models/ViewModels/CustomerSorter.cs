﻿/**
 * CustSorter.cs
 * 
 * Revision History:
 * Bao Khanh Nguyen, 2020.04.10: Created
 * 
 */

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates details of a Customer's associated sorters.
    /// </summary>
    public class CustomerSorter
    {
        public string SorterName => $"{SortContactFirstName} {SortContactLastName}";
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
        public string SortCompanyName { get; set; }
        public string SortContactFirstName { get; set; }
        public string SortContactLastName { get; set; }
        public string SortPhoneNumber { get; set; }
    }
}
