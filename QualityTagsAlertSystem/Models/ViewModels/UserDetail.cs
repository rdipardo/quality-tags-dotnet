﻿/**
 * UserDetail.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.10: Created
 */

using System.ComponentModel.DataAnnotations;

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Encapsulates the current properties of an application user's account.
    /// </summary>
    public partial class UserDetail
    {
        public string Id { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Email Confirmed?")]
        public bool HasConfirmedEmail { get; set; }

        [Display(Name = "Administrator?")]
        public bool IsAdmin { get; set; }

        [Display(Name = "Office?")]
        public bool IsOffice { get; set; }

        [Display(Name = "Creator?")]
        public bool IsCreator { get; set; }

        [Display(Name = "Locked Out?")]
        public bool IsLockedOut { get; set; }
    }
}
