﻿/**
 * TagTemplate.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.16: Created
 */

namespace QualityTagsAlertSystem.Models
{
    /// <summary>
    /// Enumerates tag categories for simple template selection.
    /// </summary>
    public enum TagTemplate
    {
        /// <summary>
        /// The default value of an uninitialized <see cref="TagTemplate"/>.
        /// </summary>
        None,
        /// <summary>
        /// The <see cref="TagTemplate"/> value of an Internal Quality Alert.
        /// </summary>
        QualityAlertInternal,
        /// <summary>
        /// The <see cref="TagTemplate"/> value of an External Quality Alert.
        /// </summary>
        QualityAlertExternal,
        /// <summary>
        /// The <see cref="TagTemplate"/> value of a Hold Tag.
        /// </summary>
        Hold,
        /// <summary>
        /// The <see cref="TagTemplate"/> value of a Temporary Process Change
        /// </summary>
        TPC,
        /// <summary>
        /// The <see cref="TagTemplate"/> value of a Special INstruction
        /// </summary>
        Special
    }
}
