﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class QualityIssues
    {
        public int Id { get; set; }
        public string ProblemType { get; set; }
        public int? Year { get; set; }
        public DateTime Date { get; set; }
        public string PartId { get; set; }
        public string IssuedBy { get; set; }
        public string OpertionHd { get; set; }
        public string OpertionTp { get; set; }
        public string OpertionSp { get; set; }
        public string OpertionQa { get; set; }
        public string Reason { get; set; }
        public string ReasonNote { get; set; }
        public string Feature { get; set; }
        public string Changed { get; set; }
        public string Comment { get; set; }
        public string SpecialInst { get; set; }
        public string QualityAlert { get; set; }
        public string QualityAlertMemo { get; set; }
        public decimal? Cost { get; set; }
        public int? NofPiecesQa { get; set; }
        public int? NofPiecesHt { get; set; }
        public int? MachineId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerRefNum { get; set; }
        public DateTime? DateIssued { get; set; }
        public string DispositionCustomer { get; set; }
        public string DispositionShipper { get; set; }
        public double LengthOfChange { get; set; }
        public string OkdBy { get; set; }
        public bool? ModWritten { get; set; }
        public bool? SpecialInstWritten { get; set; }
        public bool? HoldTag { get; set; }
        public bool? TpcTag { get; set; }
        public bool? CertTag { get; set; }
        public bool? QualityAtag { get; set; }
        public bool? Closed { get; set; }
        public bool? SupplierIssue { get; set; }
        public bool? ControlPlan { get; set; }
        public bool? LayeredAudit { get; set; }
        public bool? QualityMemo { get; set; }
        public string FeatureNumber { get; set; }
        public double? CurrentMinSpec { get; set; }
        public double? CurrentMaxSpec { get; set; }
        public double? TpcMinSpec { get; set; }
        public double? TpcMaxSpec { get; set; }
        public string OperationNumber { get; set; }
        public string Body { get; set; }
        public string Operation { get; set; }
        public byte[] Picture { get; set; }
        public int? Picture01 { get; set; }
        public int? Picture02 { get; set; }
        public int? Attachments { get; set; }
        public int? Qty { get; set; }
        public int? ActiveStatus { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
