﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class SignedOffHoldTags
    {
        public int HtsoId { get; set; }
        public int? TagNumber { get; set; }
        public DateTime? DateClosed { get; set; }
        public string OkToProcessBy { get; set; }
        public int? SortAmt { get; set; }
        public string SortBy { get; set; }
        public int? ReworkAmt { get; set; }
        public string ReworkBy { get; set; }
        public int? ScrapAmt { get; set; }
        public string ScrapBy { get; set; }
        public int? UseAsIsAmt { get; set; }
        public string UseAsIsBy { get; set; }
        public string AuthorizedBy { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
