﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class CustomerSalesReps
    {
        public int CustSalesId { get; set; }
        public int? CustomerId { get; set; }
        public string SalesRepFirstName { get; set; }
        public string SalesRepLastName { get; set; }
        public string SalesRepPhoneNumber { get; set; }
        public string SalesRepEmail { get; set; }
    }
}
