﻿using System;
using System.Collections.Generic;

namespace QualityTagsAlertSystem.Models
{
    public partial class CustomerDispositions
    {
        public int Id { get; set; }
        public string Disposition { get; set; }
    }
}
