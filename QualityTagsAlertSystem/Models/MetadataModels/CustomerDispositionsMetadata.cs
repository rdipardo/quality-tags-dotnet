﻿/**
 * CustomerDispositionsMetadata.cs
 * 
 * Useful properties for faster queries on the Customer Dispositions table.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.18: Created
 */

using System.Linq;

namespace QualityTagsAlertSystem.Models
{
    public partial class CustomerDispositions
    {
        /// <summary>
        /// Collection of Primary Keys identifying shipper dispositions.
        /// </summary>
        public readonly int[] SHIPPER_DISPOSITION_IDS = Enumerable.Range(1, 6).ToArray();

        /// <summary>
        /// Collection of  Primary Keys identifying customer dispositions.
        /// </summary>
        public readonly int[] CUSTOMER_DISPOSITION_IDS = Enumerable.Range(1, 23).ToArray();
    }
}
