﻿/**
 * AttachmentsController.cs
 * 
 * Handles file uploads
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.02: Created
 *     Chackaphope Ying Yong, 2020.03.16: Added documentation 
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QualityTagsAlertSystem.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QualityTagsAlertSystem.Helpers.UploadHelper;

namespace QualityTagsAlertSystem.Controllers
{
    /// <summary>
    /// File upload handler.
    /// </summary>
    public class AttachmentsController : Controller
    {
        /// <summary>
        /// A reference to the underlying database, encapsulated as an instance of <see cref="QualityTagsContext"/>.
        /// </summary>
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Provides information about web hosting environment an application is running in
        /// IWebHostEnvionment is the interface that has WebRootFileProvider and WebRootPath, it is the host, used in the context portion of the controller
        /// called Attachment controller
        /// IWebHostEnvironment inherits from IHostEnvironment interface, it contains strings such as application name, ContenetRootFileProvider, content root
        /// path, and environment name.
        /// </summary>
        private readonly IWebHostEnvironment _host;

        /// <summary>
        /// This interface readonly function to read the StorageLocation model
        /// </summary>
        private readonly IEnumerable<StorageLocations> _savedDirectories;

        /// <summary>
        /// Upload path name.
        /// </summary>
        private readonly string _uploadPath;

        /// <summary>
        /// Field created for storage location model, I guess by the name it is the target directory the person wants to store it in
        /// </summary>
        private StorageLocations _targetDirectory;

        /// <summary>
        /// Instantiates a new AttachmentsController and initializes the save directory and upload paths
        /// </summary>
        /// <param name="context"> context comes from Quality tag contexts model</param>
        /// <param name="host"> part of the web hosting interface</param>
        public AttachmentsController(QualityTagsContext context, IWebHostEnvironment host)
        {
            _context = context;
            _host = host;
            _uploadPath = Path.Combine(_host.WebRootPath, "storage");
            _savedDirectories = _context.StorageLocations.AsEnumerable();
        }

        /// <summary>
        /// Handles an AJAX request containing a collection of file paths to files
        /// </summary>
        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var files = Request.Form.Files;
            var storagePath = Request.Query["path"].FirstOrDefault();
            string directoryPath = string.Empty;

            if (files == null)
            {
                return Json(new { Message = "No files received!", Error = nameof(ArgumentNullException) });
            }

            if (int.TryParse(storagePath, out int storagePathId))
            {
                _targetDirectory = await _context.FindAsync<StorageLocations>(storagePathId);
                directoryPath = (_targetDirectory != null) ? _targetDirectory.Path : "";
            }

            if (string.IsNullOrEmpty(directoryPath))
            {
                directoryPath = Path.Combine(_uploadPath, Guid.NewGuid().ToString());
                var newStoragePath = _context.StorageLocations.Add(new StorageLocations { Path = directoryPath });
                await _context.SaveChangesAsync();
                _targetDirectory = await _context.FindAsync<StorageLocations>(newStoragePath.Entity.Id);
            }


            if (_targetDirectory == null)
            {
                return Json(new { Message = "Failed to create storage directory!", Error = nameof(IOException) });
            }
            else if (!Directory.Exists(_targetDirectory.Path))
            {
                Directory.CreateDirectory(_targetDirectory.Path);
            }

            foreach (var file in files)
            {
                string ext = Path.GetExtension(file.FileName).ToLower();
                string key = string.Empty;

                if (file.Length == 0)
                {
                    return Json(new { Message = $"Empty file: {file.FileName}!", AttachmentPath = _targetDirectory.Id, Error = nameof(ArgumentException) });
                }
                else if (file.Length > MAX_FILE_SIZE)
                {
                    return Json(new { Message = $"File {file.FileName} greater than max allowed size!", AttachmentPath = _targetDirectory.Id, Error = nameof(InvalidDataException) });
                }
                else if (!ACCEPTED_FILE_TYPES.Any(type => type == ext))
                {
                    return Json(new { Message = $"Extension of file {file.FileName} is not one of {string.Join(", ", ACCEPTED_FILE_TYPES)}.", AttachmentPath = _targetDirectory.Id, Error = nameof(ArgumentException) });
                }

                try
                {
                    var existingUpload = FindAttachmentByFileName(_context.Attachments.AsEnumerable(), file.FileName);

                    if (existingUpload != null)
                    {
                        key = existingUpload.Key;
                    }

                    if (string.IsNullOrEmpty(key))
                    {
                        key = Guid.NewGuid().ToString() + ext;
                    }

                    string filePath = Path.Combine(_targetDirectory.Path, key);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    if (existingUpload == null)
                    {
                        Attachments attachment = new Attachments
                        {
                            Path = _targetDirectory.Id,
                            Key = key,
                            Filename = file.FileName.Trim()
                        };

                        _context.Attachments.Add(attachment);
                    }
                    else
                    {
                        existingUpload.Path = _targetDirectory.Id;
                        existingUpload.Key = key;
                        existingUpload.Filename = file.FileName.Trim();

                        _context.Update(existingUpload);
                    }

                    await _context.SaveChangesAsync();
                }
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    return Json(new { Message = $"Error: {exc.Message}", AttachmentPath = _targetDirectory.Id, Error = exc.GetType().Name });
                }
            }

            return Json(new { Message = "Upload successful", AttachmentPath = _targetDirectory.Id });
        }

        /// <summary>
        /// This is the Index proportion of the controller. It draws the model from the qualityTagContext and returns a view listing the index
        /// of all the quality tag context
        /// </summary>
        /// <returns>returns the view of the index view for this controller</returns>
        public async Task<IActionResult> Index()
        {
            var qualityTagsContext = _context.Attachments.Include(a => a.PathNavigation);
            return View(await qualityTagsContext.ToListAsync());
        }

        /// <summary>
        /// this is the details portion of the controller which sends the user to the details of the specific part they want to see the details on
        /// </summary>
        /// <param name="id">this is the id of the specific quality tag in the qualityTagContexts</param>
        /// <returns>view of the detail of the part they selected to view</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var attachments = await _context.Attachments
                .Include(a => a.PathNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (attachments == null)
            {
                return NotFound();
            }

            return View(attachments);
        }

        /// <summary>
        /// this is the delete portion of the controller, it will find the id of the attachment and will bring the user to the delete view
        /// </summary>
        /// <param name="id">id of the attachment</param>
        /// <returns> View showing details of model to be deleted.</returns>
        [Authorize(Roles = "admin, creator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var attachments = await _context.Attachments
                .Include(a => a.PathNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (attachments == null)
            {
                return NotFound();
            }

            return View(attachments);
        }

        /// <summary>
        /// this is the post-back method used when the user clicks the delete button, it will remove the attachment and send the user back to 
        /// the index view
        /// </summary>
        /// <param name="id">id of the attachment</param>
        [Authorize(Roles = "admin, creator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var attachments = await _context.Attachments.FindAsync(id);
            _context.Attachments.Remove(attachments);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Checks if an attachment record with the given id exists.
        /// </summary>
        /// <param name="id"> Primary key of an attachment.</param>
        /// <returns> True if the record exists.</returns>
        private bool AttachmentExists(int id)
        {
            return _context.Attachments.Any(e => e.Id == id);
        }
    }
}
