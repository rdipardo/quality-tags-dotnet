﻿/**
 * EmailsController.cs
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 */

using QualityTagsAlertSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace QualityTagsAlertSystem.Controllers
{
    /// <summary>
    /// Utility Controller that renders email content as HTML.
    /// </summary>
    /// <remarks>
    /// Implementation adapted from Hasan A. Yousef and Glorfindel's answer to <see href="https://stackoverflow.com/questions/40912375/return-view-as-string-in-net-core"/>.
    /// </remarks>
    [AllowAnonymous]
    public class EmailController : ControllerBase
    {
        /// <summary>
        /// A reference to an instance of the <see cref="IViewRenderService"/> interface.
        /// </summary>
        private readonly IViewRenderService _viewRenderService;

        /// <summary>
        /// Constructs a new <see cref="EmailController"/> from the given instance of the <see cref="IViewRenderService"/> interface.
        /// </summary>
        /// <param name="viewRenderService"> An initialized instance of the <see cref="IViewRenderService"/> interface.</param>
        public EmailController(IViewRenderService viewRenderService)
        {
            _viewRenderService = viewRenderService;
        }

        /// <summary>
        /// Calls the implementation of <see cref="IViewRenderService.RenderToStringAsync{TModel}(string, TModel)"/> provided by <see cref="ViewRenderService"/>.
        /// </summary>
        /// <seealso cref="ViewRenderService.RenderToStringAsync{TModel}(string, TModel)"/>
        public async Task<string> FetchBody<TModel>(string viewName, TModel model) => await _viewRenderService.RenderToStringAsync(viewName, model);
    }
}
