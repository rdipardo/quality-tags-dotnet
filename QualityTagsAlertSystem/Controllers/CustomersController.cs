﻿/**
 * CustomersController.cs
 *
 * Provides a CRUD interface for managing Customer domain objects.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.15: Created
 *     Xinghua Li, 2020.04.09: Implemented Contacts Action
 *     Bao Khanh Nguyen, 2020.04.10 : Sorters Actions
 */

using QualityTagsAlertSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static System.String;

namespace QualityTagsAlertSystem.Controllers
{
    /// <summary>
    /// Manages <see cref="CustomersController"/> business objects.
    /// </summary>
    public class CustomersController : Controller
    {
        /// <summary>
        /// A reference to the underlying database, encapsulated as an instance of <see cref="QualityTagsContext"/>.
        /// </summary>
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Contrsusts a new instance of <see cref="CustomersController"/>.
        /// </summary>
        /// <param name="context"> An initialized instance of <see cref="QualityTagsContext"/>.</param>
        public CustomersController(QualityTagsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Serves a listing of saved Customer contacts.
        /// </summary>
        [Authorize(Roles = "admin, office, creator")]
        public async Task<IActionResult> Contacts()
        {
            // a LINQ query for customer information
            // LEFT JOINs are simulated by `<table>.DefaultIfEmpty()`
            // see https://docs.microsoft.com/en-us/dotnet/csharp/linq/perform-left-outer-joins
            var customerContacts = (from customer in _context.Customers
                                    join contact in _context.CustomerContacts.DefaultIfEmpty()
                                        on customer.CustomerId equals contact.CustomerId
                                    join salerep in _context.CustomerSalesReps.DefaultIfEmpty()
                                        on customer.CustomerId equals salerep.CustomerId
                                    join sorter in _context.CustomerSorters.DefaultIfEmpty()
                                        on customer.CustomerId equals sorter.CustomerId
                                    select new CustomerContact()
                                    {
                                        CustomerId = customer.CustomerId,
                                        CustName = customer.CustName,
                                        CustLocation = customer.CustLocation,
                                        Current = customer.Current,

                                        ContactFirstName = contact.ContactFirstName,
                                        ContactLastName = contact.ContactLastName,
                                        ContactPhoneNumber = contact.PhoneNumber,
                                        ContactCellNumber = contact.CellNumber,

                                        SalesRepFirstName = salerep.SalesRepFirstName,
                                        SalesRepLastName = salerep.SalesRepLastName,
                                        SalesRepPhoneNumber = salerep.SalesRepPhoneNumber,
                                    })
                                    .AsEnumerable()
                                    .Where(c => c.Current.GetValueOrDefault() && !IsNullOrEmpty(c.ContactFirstName))
                                    .OrderBy(c => c.CustName)
                                    .ThenBy(c => c.CustLocation)
                                    .GroupBy(c => c.CustName)
                                    .Select(cc => cc)
                                    .ToList();

            return View(await Task.Run(() => customerContacts));
        }

        /// <summary>
        /// Serves a listing of saved Customer sorters.
        /// </summary>
        [Authorize(Roles = "admin, office, creator")]
        public async Task<IActionResult> Sorters()
        {
            var custSorter = (from customer in _context.Customers
                              join contact in _context.CustomerContacts
                                  on customer.CustomerId equals contact.CustomerId
                              join salerep in _context.CustomerSalesReps
                                  on customer.CustomerId equals salerep.CustomerId
                              join sorter in _context.CustomerSorters
                                  on customer.CustomerId equals sorter.CustomerId
                              select new CustomerSorter()
                              {
                                  CustomerId = customer.CustomerId,
                                  CustName = customer.CustName,
                                  CustLocation = customer.CustLocation,
                                  SortCompanyName = sorter.SortCompanyName,
                                  SortContactFirstName = sorter.SortContactFirstName,
                                  SortContactLastName = sorter.SortContactLastName,
                                  SortPhoneNumber = sorter.SortPhoneNumber,
                                  Current = customer.Current
                              })
                            .AsEnumerable()
                            .Where(c => c.Current.GetValueOrDefault() && !IsNullOrEmpty(c.SortContactFirstName))
                            .OrderBy(c => c.CustName)
                            .ThenBy(c => c.CustLocation)
                            .GroupBy(c => c.CustName)
                            .Select(cc => cc)
                            .ToList();

            return View(await Task.Run(() => custSorter));
        }

        /// <summary>
        /// Lists all external issues, with associated customer details
        /// </summary>
        [Authorize(Roles = "admin, office, creator")]
        public async Task<IActionResult> CustomerIssues()
        {
            var customerList = _context.Customers.Select(c =>
                new Customers() { CustomerId = c.CustomerId, CustName = c.CustName, CustLocation = c.CustLocation });

            var customerPartList = from tag in _context.QualityIssues
                                   join cust in customerList.DefaultIfEmpty() on tag.CustomerId equals cust.CustomerId
                                   join part in _context.Parts on tag.PartId equals part.PartId
                                   select part;

            var issues = (from tag in _context.QualityIssues
                          join cust in customerList on tag.CustomerId equals cust.CustomerId
                          join part in customerPartList on tag.PartId equals part.PartId
                          join plant in _context.PlantNames on part.PlantNumber equals plant.PlantId
                          where tag.ProblemType.Equals("EX", StringComparison.OrdinalIgnoreCase)
                          select new CustomerIssue(tag, cust, plant))
                          .AsEnumerable()
                          .OrderByDescending(i => i.DateIssued);

            return View(await Task.Run(() => issues));
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,CustName,CustLocation,Current")] Customers tblCustomers)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblCustomers);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblCustomers);
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblCustomers = await _context.Customers.FindAsync(id);
            if (tblCustomers == null)
            {
                return NotFound();
            }
            return View(tblCustomers);
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerId,CustName,CustLocation,Current")] Customers tblCustomers)
        {
            if (id != tblCustomers.CustomerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblCustomers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomersExists(tblCustomers.CustomerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblCustomers);
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblCustomers = await _context.Customers
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            if (tblCustomers == null)
            {
                return NotFound();
            }

            return View(tblCustomers);
        }

        /// <summary>
        /// NOT YET IMPLEMENTTED
        /// </summary>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblCustomers = await _context.Customers.FindAsync(id);
            _context.Customers.Remove(tblCustomers);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Checks if a customer record with the given id exists.
        /// </summary>
        /// <param name="id"> Primary key of a customer record.</param>
        /// <returns> True if the record exists.</returns>
        private bool CustomersExists(int id)
        {
            return _context.Customers.Any(e => e.CustomerId == id);
        }
    }
}
