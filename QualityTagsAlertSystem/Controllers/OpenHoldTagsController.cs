﻿/**
 * OpenHoldTagsController.cs
 * 
 * Utility controller that queries open Hold Tags.
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.12: Created
 */

using Microsoft.AspNetCore.Mvc;
using QualityTagsAlertSystem.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityTagsAlertSystem.Controllers
{
    /// <summary>
    /// Utility controller that queries open Hold Tags.
    /// </summary>
    [Route("api/OpenTagFinder")]
    [ApiController]
    public class OpenHoldTagsController : ControllerBase
    {
        /// <summary>
        /// A reference to the underlying database, encapsulated as an instance of <see cref="QualityTagsContext"/>.
        /// </summary>
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Injects a reference to the underlying database into a new <see cref="OpenHoldTagsController"/>.
        /// </summary>
        /// <param name="context"> A reference to the encapsulated database context.</param>
        public OpenHoldTagsController(QualityTagsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Handles AJAX requests from the form API.
        /// </summary>
        /// <param name="openTag"> A JSON object mapping of a <see cref="QualityIssues"/> model instance.</param>
        [HttpPost]
        public async Task<JsonResult> GetOpenTag(QualityIssues openTag)
        {
            QualityIssues selectedTag = null;

            int tagId = openTag != null ? openTag.Id : 0;

            if (tagId > 0)
            {
                selectedTag = _context.QualityIssues.Find(tagId);
            }

            return await Task.Run(() => new JsonResult(selectedTag) { ContentType = "application/json; charset=utf-8" });
        }
    }
}
