﻿/**
 * PartValueCalculation.cs
 *
 * Provides a CRUD interface for managing Parts business objects.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.25: Created
 *     Chackaphope Ying Yong, 2020.04.10: Added documentation 
 */

using QualityTagsAlertSystem.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace QualityTagsAlertSystem.Controllers
{
    /// <summary>
    /// Manages <see cref="Parts"/> business objects.
    /// </summary>
    public class PartsController : Controller
    {
        /// <summary>
        /// A reference to the underlying database, encapsulated as an instance of <see cref="QualityTagsContext"/>.
        /// </summary>
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Constructs a new <see cref="PartsController"/> from the given instance of <see cref="QualityTagsContext"/>.
        /// </summary>
        /// <param name="context"> A reference to the encapsulated database context.</param>
        public PartsController(QualityTagsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Serves the part value calculator View.
        /// </summary>
        public async Task<IActionResult> CalculateValue()
        {
            // collect the Ids of parts marked as scrap
            var partsQuery = _context.Parts
                            .AsEnumerable()
                            .Where(p => p.RptScrap.GetValueOrDefault())
                            .OrderBy(p => p.PartId);

            var calculatedValues = from partDept in _context.PartDepts
                                   join part in _context.Parts on partDept.PartId equals part.PartId
                                   join dept in _context.Depts on partDept.DeptId equals dept.DeptId
                                   where partDept.PartId.Equals(partsQuery.FirstOrDefault().PartId, StringComparison.OrdinalIgnoreCase)
                                            && !dept.Department.Equals("Blending", StringComparison.OrdinalIgnoreCase)
                                   select new PartValueCalculation()
                                   {
                                       PartDeptId = part.PartId,
                                       Department = dept.Department,
                                       Cost = partDept.Cost,
                                       WeightLbs = partDept.WeightLbs,
                                       TotalValue = 1.0m * partDept.Cost, // decimal
                                       TotalWeight = 1.0 * partDept.WeightLbs, // double
                                       RptScrap = part.RptScrap,
                                       DepartmentState = dept.DepartmentState
                                   };

            // TODO 
            // pass a  generic Collection and fill an HTML `datalist` instead:
            // ViewBag.PartsList = partsQuery;
            ViewBag.PartsList = new SelectList(partsQuery, "PartId", "PartId");

            return View(await Task.Run(() => calculatedValues));
        }

        /// <summary>
        /// Servers a complete part listing.
        /// </summary>
        public async Task<IActionResult> Index()
        {
            return View(await _context.Parts.ToListAsync());
        }

        /// <summary>
        /// Serves a summary of the part identified by the given id
        /// </summary>
        /// <param name="id"> Primary key of a Part.</param>
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parts = await _context.Parts
                .FirstOrDefaultAsync(m => m.PartId == id);
            if (parts == null)
            {
                return NotFound();
            }

            return View(parts);
        }

        /// <summary>
        /// Serves the Part insert form.
        /// </summary>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Handles POST requests from the Part insert form.
        /// </summary>
        /// <param name="parts"> An initialized instance of the <see cref="Parts"/> model.</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PartId,Description,RptScrap,PlantNumber")] Parts parts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(parts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(parts);
        }

        /// <summary>
        /// Serves the edit form for the Part identified by the given id.
        /// </summary>
        /// <param name="id"> Primary key of a Part.</param>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parts = await _context.Parts.FindAsync(id);
            if (parts == null)
            {
                return NotFound();
            }
            return View(parts);
        }

        /// <summary>
        /// Handles POST requests from the Part edit form.
        /// </summary>
        /// <param name="id"> Primary key of a Part to be edited.</param>
        /// <param name="parts"> An initialized instance of the <see cref="Parts"/> model.</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("PartId,Description,RptScrap,PlantNumber")] Parts parts)
        {
            if (id != parts.PartId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(parts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PartsExists(parts.PartId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(parts);
        }

        /// <summary>
        /// Serves the delete form for the Part identified by the given id.
        /// </summary>
        /// <param name="id"> Primary key of a Part.</param>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parts = await _context.Parts
                .FirstOrDefaultAsync(m => m.PartId == id);
            if (parts == null)
            {
                return NotFound();
            }

            return View(parts);
        }

        /// <summary>
        /// Handles POST requests from the Part delete form.
        /// </summary>
        /// <param name="id"> Primary key of the Part to be deleted.</param>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var parts = await _context.Parts.FindAsync(id);
            _context.Parts.Remove(parts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Returns <see langword="true"/> if a Part exists with the given id.
        /// </summary>
        /// <param name="id"> Primary key of a Part.</param>
        /// <returns> <see langword="true"/> if the Part record exists.</returns>
        private bool PartsExists(string id)
        {
            return _context.Parts.Any(e => e.PartId == id);
        }
    }
}
