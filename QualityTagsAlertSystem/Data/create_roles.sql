USE `quality_tags_users`;

INSERT INTO `AspNetRoles` (
	`Id`
	,`Name`
	,`NormalizedName`
	,`ConcurrencyStamp`
	)
VALUES (
	-- all access (DG 17/03/2020)
	UUID()
	,'admin'
	,UPPER('admin')
	,UUID()
	)
	,
	-- engineers in office can do most things except create/edit/delete support table entries. We don't want out of control pick list entries (DG 17/03/2020)
	(
	UUID()
	,'office'
	,UPPER('office')
	,UUID()
	)
	,
	-- plant floor create and maybe edit what they created (DG 17/03/2020)
	(
	UUID()
	,'creator'
	,UPPER('creator')
	,UUID()
	);

-- assign first user to `admin` role for demo purposes
INSERT INTO `AspNetUserRoles` (
	`UserId`
	,`RoleId`
	)
VALUES (
	(
		SELECT `Id`
		FROM `AspNetUsers` LIMIT 1
		)
	,(
		SELECT `Id`
		FROM `AspNetRoles` LIMIT 1
		)
	);
