/* site.js
 *
 * Global helper functions, event handler initialization
 *
 * Revision History
 *     Robert Di Pardo, 2020.02.28: Created
 */

/**
 * Returns a Javascript `Array` from any iterable object.
 * @param {jQuery} collection An iterable object returned by a jQuery selector.
 * @returns {Array<any>} An instance of a Common JS `Array`.
 */
function toArray(collection) {
    return Array.prototype.slice.call(collection);
}

/**
 * Reports status messages with the appropriate Bootstrap styling.
 * @param {string} msg Status message.
 * @param {boolean} err Is the message reporting an error?
 */
function flashMessage(msg, err) {
    $('#tempDataMessage').text(msg);
    $('#tempDataMessage').parent().show();
    if (err)
        $('#tempDataMessage').parent().addClass('alert-danger');
    else
        $('#tempDataMessage').parent().removeClass('alert-danger');
    window.scrollTo(0, 0);
}

/**
 * Initialization
 */
$(function () {
    $(".alert").find('.message').each(function () {

        // only show alerts that contain message text
        if ($(this).text().trim().length) {

            // set an appropriate style for error messages
            if (new RegExp(/^((.*ERR.*)|(.*FAIL.*)|(.*WARN.*)|(.*DANGER.*)|(.*BAD.*))$/i).test($(this).text().trim())) {
                $(this).parent().addClass('alert-danger');
            }
            $(this).parent().show();
        } else {
            $(this).parent().hide();
            $(this).parent().removeClass('alert-danger');
        }
    });
});