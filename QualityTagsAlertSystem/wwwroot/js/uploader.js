/**
 * uploader.js
 *
 * Emits AJAX requests to file uploading actions
 *
 * Revision History
 * 	Robert Di Pardo, 2020-02-26: Created
 * 	"		" , 				 2020-03-01: Refactored `success` callback
 */

function uploadFilesAsync(form, evt) {

	const submitEvent = evt || window.event;
	const inputControl = submitEvent.target;
	let fileList;

	submitEvent.preventDefault();

	try {
		fileList = $(form).find('input[type=file]');
	} catch (e) {
		flashMessage('No file input available!', true);
		return;
	}

	if (fileList.length > 0) {
		const data =
			toArray(fileList).reduce((data, upload) => {
				if (upload.files) {
					toArray(upload.files).forEach(file => data.append(file.name, file));
				}
				return data;

			}, new FormData());

		const controllerAction = /^((.*photo.*)|(.*pic.*))$/i.test(inputControl.id)
								? encodeURI('/Blobs/Upload?path=' + $('#picturePath').val())
								: encodeURI('/Attachments/Upload?path=' + $('#attachmentPath').val());

		$.ajax({
			type: "POST",
			url: controllerAction,
			contentType: false,
			processData: false,
			data: data,
			success: function (result) {
				if (!result.error) {
					$('#photo01').val($('#photoUpload01').val().replace(/^.*[\\\/]/, ''));
					$('#photo02').val($('#photoUpload02').val().replace(/^.*[\\\/]/, ''));
				} else {
					inputControl.value = '';
				}

				if (result.picturePath) {
					$('#picturePath').val(result.picturePath);
				}

				if (result.attachmentPath) {
					$('#attachmentPath').val(result.attachmentPath);
					$('#Attachments').val(result.attachmentPath);
				}

				flashMessage(result.message, result.error);
				console.error(result.error);
			},
			error: function (jqXHR, textStatus) {
				flashMessage(textStatus.toUpperCase() + ": " + jqXHR.status, true);
				inputControl.value = '';
			}
		});
	}
}
