﻿/**
 * UploadHelper.cs
 * 
 * Constants, utility methods for the upload Controllers
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.02: Created
 */

using QualityTagsAlertSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QualityTagsAlertSystem.Helpers
{
    /// <summary>
    /// Encapsulates accepted file types, utility methods for use by the
    /// <see cref="QualityTagsAlertSystem.Controllers.AttachmentsController"/> 
    /// and <see cref="QualityTagsAlertSystem.Controllers.BlobsController"/>.
    /// </summary>
    public static class UploadHelper
    {
        /// <summary>
        /// The maximum allowed byte count of file uploads.
        /// </summary>
        public static readonly long MAX_FILE_SIZE = 10 * 1024 * 1024;

        /// <summary>
        /// The extensions of allowed picture file uploads.
        /// </summary>
        public static readonly string[] ACCEPTED_PICTURE_TYPES = { ".jpg", ".jpeg", ".png", ".gif" };

        /// <summary>
        /// The extensions of allowed document uploads.
        /// </summary>
        public static readonly string[] ACCEPTED_FILE_TYPES = { ".xls", ".xlsx", ".csv", ".pdf", ".doc", ".docx", ".odt", ".rtf", ".htm", ".html", ".xml", ".json", ".yml", ".yaml", ".txt", ".log" };

        /// <summary>
        /// Queries the given collection of <see cref="Blobs"/> objects for the picture with the given <paramref name="filename"/>.
        /// </summary>
        /// <param name="pictures"> A collection of <see cref="Blobs"/> objects.</param>
        /// <param name="filename"> The name of a saved picture file with the given <paramref name="filename"/>.</param>
        /// <returns> The matching <see cref="Blobs"/> object, if any.</returns>
        public static Blobs FindPictureByFileName(IEnumerable<Blobs> pictures, string filename)
        {
            return pictures.FirstOrDefault(p =>
                            p.Filename
                            .Equals(filename, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Queries the given collection of <see cref="Attachments"/> objects for the picture with the given <paramref name="filename"/>.
        /// </summary>
        /// <param name="docs"> A collection of <see cref="Attachments"/> objects.</param>
        /// <param name="filename"> The name of a saved document file with the given <paramref name="filename"/>.</param>
        /// <returns> The matching <see cref="Attachments"/> object, if any.</returns>
        public static Attachments FindAttachmentByFileName(IEnumerable<Attachments> docs, string filename)
        {
            return docs.FirstOrDefault(d =>
                            d.Filename
                            .Equals(filename, StringComparison.OrdinalIgnoreCase));
        }
    }
}
