# Quality Tags Alert System

[![License: Apache-2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

### Contents

- [Database configuration](#markdown-header-setting-up-the-database)
- [SMTP client configuration](#markdown-header-smtp-client-settings)
- [Deploying with Docker](#markdown-header-deployment)
- [Initializing user accounts and roles](#markdown-header-user-accounts)

### Features

- customized user authentication rules
- user account management without leaving the app
- save, edit quality alert reports (*a full suite of CRUD actions is forthcoming*)
- deployable as a web container with its own `postfix` mail transfer agent

### Setting up the database

#### Connection strings

**Notes.**

- The database service container is reachable on `localhost` when the value of the `services.web.network_mode` key is `"host"` in the
  [docker-compose file](docker/compose.yml). This is already configured for easier setup.

- When `services.web.network_mode` is *not* set to `"host"` (i.e., in bridged network mode), the database service listens on a dynamic
  IP address; it can be found at runtime time with:

```
      docker inspect docker-db-1 | jq '.[].NetworkSettings.Networks' | grep IPAddress
```

  See the comments in the [Dockerfile](docker/Dockerfile) for guidance on troubleshooting an unreachable service container.

- Use the **same** property names as below (they must match the keys found in [Startup.cs](QualityTagsAlertSystem/Startup.cs#lines-48)):

  **Note.**
  For `DOCKER_HOST_ADDRESS`, substitute either `localhost` (in "host" network mode), or the internal IP of the database service container
  (bridged network mode).

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_alert_system;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360",
    "IdentityStoresConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_users;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360"
  }
}
```

- go to `http://localhost:6117`
- log in to server `db` as `MYSQL_ROOT_USER` using `MYSQL_ROOT_PASSWORD`
- import the [schemas and data](QualityTagsAlertSystem/Data/scaffold.sql) by file upload
- if you prefer using a standard account in place of `root`, change `MYSQL_USER` to the name of a standard user and execute:

```mysql
GRANT SELECT, INSERT, UPDATE, DELETE, INDEX, REFERENCES ON `quality_tags_alert_system`.* TO 'MYSQL_USER'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE, INDEX, REFERENCES, CREATE ON `quality_tags_users`.* TO 'MYSQL_USER'@'%';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'MYSQL_USER'@'%';
```

### SMTP client settings

- use the same property names as below (see [Startup.cs](QualityTagsAlertSystem/Startup.cs#lines-114))

```json
  "EmailConfiguration": {
    "SmtpServer": "/* SMTP_DOMAIN */",
    "SmtpPort": "25 | 587 | 465",
    "SmtpUsername": "/* ACCOUNT_EMAIL_ADDRESS */",
    "SmtpPassword": "/* ACCOUNT_PASSWORD | API_KEY */",
    "MailerName": "/* NAME_TO_PUT_ON_OUTGOING_MAIL */",
    "MailerAddress": "/* EMAIL_TO_SEND_FROM */"
  }
```

#### How many parameters will you have to set?

It depends on the kind of relay your mailer uses for delivery:

|              |  e.g.                     |SmtpServer|SmtpPort|MailerName|MailerAddress|SmtpUserName|SmtpPassword|
|--------------|---------------------------|:--------:|:------:|:--------:|:-----------:|:----------:|:----------:|
| local relay  |`sendmail`,`exim`,`postfix`| X        | X      | X        | X           |            |            |
| remote relay |`smtp.gmail.com`           | X        | X      | X        |             | X          | X          |

Basically, if your mail agent stores credentials on the host machine, ignore `SmtpUserName` and `SmtpPassword`.

If using a third-party e-mail service, provide your account e-mail and password as `SmtpUserName` and `SmtpPassword`, respectively.

##### Caution
> If using a remote relay, your `SmtpUserName` **must** be a valid e-mail address. The mailer will prefer this to any `MailerAddress` you provide

### Deployment

Put all configuration values together in `appsettings.json` and save it to the main project folder: (`QualityTagsAlertSystem/`), e.g.:

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_alert_system;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360",
    "IdentityStoresConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_users;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360"
  },
  "EmailConfiguration": {
    "SmtpServer": "/* SMTP_DOMAIN */",
    "SmtpPort": "25 | 587 | 465",
    "SmtpUsername": "/* ACCOUNT_EMAIL_ADDRESS */",
    "SmtpPassword": "/* ACCOUNT_PASSWORD | API_KEY */",
    "MailerName": "/* NAME_TO_PUT_ON_OUTGOING_MAIL */",
    "MailerAddress": "/* EMAIL_TO_SEND_FROM */"
  },
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
```

- provide valid [environment variables](docker/example.env) for the database and SMTP relay; save them as`.env` in the `docker` directory
- if your firewall is running, open the inbound port to the database service, modifying the port number as needed:

```
sudo nft flush table nat
sudo nft add table nat
sudo nft 'add chain nat prerouting { type nat hook prerouting priority 0; }'
sudo nft "add rule nat prerouting iifname docker0 tcp dport { 13306 } dnat to $DOCKER_HOST_ADDRESS:13306"
```

- make sure you're in the **repository** root, i.e.

```
$ ls -gGo
total 20
drwxrwx--- 1  8192 Apr 13 07:29 QualityTagsAlertSystem
drwxrwx--- 1  4096 Apr 11 21:36 QualityTagsAlertSystem.Tests
-rwxrwx--- 1 11558 Mar 11 07:10 LICENSE.txt
-rwxrwx--- 1   301 Apr 12 07:10 NOTICE.txt
-rwxrwx--- 1  7305 Apr 13 07:34 README.md
drwxrwx--- 1  4096 Apr 13 07:37 docker
drwxrwx--- 1  4096 Apr 13 07:20 docs
```

- start the services

```bash
docker-compose -f docker/compose.yml up -d
```

### Run tests

Set the `build.args.TEST_BUILD` key to `true` in the [docker-compose file](docker/compose.yml):

```yml
services:
  web:
    container_name: quality-tags-app-host
    # . . .
    build:
      args:
        # . . .
        TEST_BUILD: true
    # . . .
```

Then build or rebuild the application image.

### User accounts

- go to `http://localhost:6117`

- create the `quality_tags_users` database if it doesn't already exist

- from the `QualityTagsAlertSystem` source directory, create the user database schema with the [Entity Framework CLI tool][ef-cli]:

      dotnet-ef database update --context UsersContext

**Note**. Make sure that `QualityTagsAlertSystem/appsettings.json` defines a valid `"IdentityStoresConnection"`
          [connection string](#markdown-header-setting-up-the-database) before attempting this step.

- open a browser to the application and register as a new user. Successful registration will be confirmed by a page like this:

![new-registration-screen](docs/img/new-registration-screen.png)

- if you've set up the SMTP client, the email goes to the address you signed up with:

![new-registration-email-received](docs/img/new-registration-email-received.png)

**Note**. If you own the SMTP server, we recommend looking in the *Sent* folder for your confirmation before making a bug report.

- the provided link returns you to the site:

![](docs/img/new-registration-confirmed.png)

### Logging in

- return to `http://localhost:6117`
- create the user roles and add the first user (i.e. yourself) to the `admin` role by uploading or executing [the provided script](QualityTagsAlertSystem/Data/create_roles.sql):

- if you're an admin user, you should see the full complement of links:

![logged-in-user](docs/img/all-access.png)

- all other users will be denied the **Site Administration** heading

## License

Copyright 2020 Robert Di Pardo and Contributors

Distributed under the terms of the Apache License, Version 2.0.
More information can be found [here](http://www.apache.org/licenses/LICENSE-2.0).

[Apache-2.0-badge]: https://img.shields.io/badge/License-Apache%202.0-blue.svg
[Apache-2.0]: https://opensource.org/licenses/Apache-2.0
[ef-cli]: https://learn.microsoft.com/en-us/ef/core/cli/dotnet#dotnet-ef-database-update