FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build

WORKDIR /app
COPY QualityTagsAlertSystem/ ./
RUN dotnet build --nologo QualityTagsAlertSystem.csproj -c Release -o /app/build

FROM build AS publish
RUN dotnet publish --nologo QualityTagsAlertSystem.csproj -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim

WORKDIR /app
COPY --from=publish /app/publish .

CMD ["dotnet", "QualityTagsAlertSystem.dll"]