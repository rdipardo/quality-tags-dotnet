FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

ARG BUILD_MODE=Release
ARG TEST_BUILD=false

WORKDIR /app
COPY QualityTagsAlertSystem/ ./QualityTagsAlertSystem
COPY QualityTagsAlertSystem.Tests/ ./test
RUN dotnet build --nologo QualityTagsAlertSystem/QualityTagsAlertSystem.csproj -c $BUILD_MODE -o /app/build
RUN if [ "$TEST_BUILD" = "true" ]; then \
      cd /app/test ; \
      dotnet test --nologo QualityTagsAlertSystem.Tests.csproj -c $BUILD_MODE ; \
    fi

FROM build AS publish
RUN dotnet publish --nologo QualityTagsAlertSystem/QualityTagsAlertSystem.csproj -c $BUILD_MODE -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:6.0

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true
RUN apt-get -qq update
RUN echo "postfix postfix/mailname string $(hostname -f)" | debconf-set-selections \
  && echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections \
  && apt-get -qqy --no-install-recommends install libsasl2-modules postfix \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
RUN echo 'maillog_file=/var/log/mail.log' >> /etc/postfix/main.cf

# PLEASE READ THIS
# ================
# For workarounds to Docker's default bridge network, see https://docs.docker.com/network/bridge/
# If you encounter `SmtpCommandException 5.7.1 <recipient@domain.com>: Relay access denied`, it probably means
# your browser is calling from Docker's bridge IP, which can only be seen when services are running:
#
#      ~$> ifconfig
#          br-de119aaa269d Link encap:Ethernet  HWaddr 02:42:eb:78:7c:06
#            inet addr:172.30.0.1  Bcast:172.30.255.255  Mask:255.255.0.0
#
# The client MUST BE OPEN to the range where _THIS_ IP is found, so:
# RUN sed -i '/^mynetworks*/c\mynetworks = 172.30.0.0/16 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128' /etc/postfix/main.cf

# See:
#  - docker/example.env
#  - README.md#deployment
#  - https://serverfault.com/questions/325955/no-worthy-mechs-found-when-trying-to-relay-email-to-gmail-using-postfix
#  - https://stackoverflow.com/questions/18318789/linux-postfix-dovecot-554-relay-access-denied#18330793
#  - https://www.cyberciti.biz/faq/how-to-configure-postfix-relayhost-smarthost-to-send-email-using-an-external-smptd/

RUN sed -i "/relayhost*/c\relayhost = ${RELAY_HOST}" /etc/postfix/main.cf
RUN echo 'smtp_use_tls = yes' >> /etc/postfix/main.cf
RUN echo 'smtp_sasl_auth_enable = yes' >> /etc/postfix/main.cf
RUN echo 'smtp_always_send_ehlo = yes' >> /etc/postfix/main.cf
RUN echo 'smtp_sasl_security_options = noanonymous' >> /etc/postfix/main.cf
RUN sed -i '/smtp_tls_security_level*/c\smtp_tls_security_level = encrypt' /etc/postfix/main.cf
RUN echo "smtp_sasl_password_maps = static:${SMTP_EMAIL_ADDRESS}:${SMTP_PASSWORD}" >> /etc/postfix/main.cf
# Consider changing `all` to `localhost` if spam bots are a concern
RUN sed -i "/inet_interfaces*/c\inet_interfaces = all" /etc/postfix/main.cf

WORKDIR /app
COPY --from=publish /app/publish QualityTagsAlertSystem
WORKDIR /app/QualityTagsAlertSystem

ENTRYPOINT service postfix restart && bash -c "dotnet QualityTagsAlertSystem.dll"
